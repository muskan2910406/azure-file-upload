from flask import Flask, request, render_template
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient
import os

app = Flask(__name__)

# Azure Blob Storage connection settings
account_name = "muskanstorageassignment"
account_key = "uM8LVot3yilRJOIjplFWXphjfMZ/44mk2HO2h25s5/kYimGJzGrW5QwbLAB1HdYsIu3X6h4QaYLP+AStYoR+iQ=="
container_name = "fileupload"

# Create BlobServiceClient using account name and account key
blob_service_client = BlobServiceClient(account_url=f"https://{account_name}.blob.core.windows.net/", credential=account_key)

# Create ContainerClient
container_client = blob_service_client.get_container_client(container_name)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'No file part'
        
        file = request.files['file']

        if file.filename == '':
            return 'No selected file'

        # Upload file to Azure Blob Storage
        blob_client = container_client.get_blob_client(file.filename)
        blob_client.upload_blob(file.stream.read(), overwrite=True)

        return 'File uploaded successfully'

    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)